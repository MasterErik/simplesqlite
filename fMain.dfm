object frmMain: TfrmMain
  Left = 259
  Top = 121
  Caption = 'Test work'
  ClientHeight = 614
  ClientWidth = 785
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnlLog: TPanel
    Left = 0
    Top = 565
    Width = 785
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
  end
  object pnlProgress: TPanel
    Left = 0
    Top = 595
    Width = 785
    Height = 19
    Align = alBottom
    Alignment = taLeftJustify
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object pbProgress: TProgressBar
      Left = 81
      Top = 2
      Width = 702
      Height = 15
      Align = alClient
      TabOrder = 1
    end
    object Panel4: TPanel
      Left = 2
      Top = 2
      Width = 79
      Height = 15
      Align = alLeft
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = ' Export data:'
      TabOrder = 0
    end
  end
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 785
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object pnlRight: TPanel
      Left = 0
      Top = 0
      Width = 273
      Height = 565
      Align = alClient
      BevelOuter = bvSpace
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 271
      ExplicitHeight = 563
      object pnlStatistics: TPanel
        Left = 1
        Top = 1
        Width = 271
        Height = 24
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
        Caption = 'Departments'
        TabOrder = 0
        ExplicitWidth = 269
      end
      object lvDepartments: TListView
        Left = 1
        Top = 25
        Width = 271
        Height = 539
        Align = alClient
        BorderStyle = bsNone
        Columns = <
          item
            AutoSize = True
            Caption = 'Title'
          end>
        HideSelection = False
        ReadOnly = True
        RowSelect = True
        TabOrder = 1
        ViewStyle = vsReport
        OnChange = lvDepartmentsChange
        OnCustomDrawItem = lvDepartmentsCustomDrawItem
        ExplicitWidth = 269
        ExplicitHeight = 537
      end
    end
    object pnlUserDetail: TPanel
      Left = 273
      Top = 0
      Width = 512
      Height = 565
      Align = alRight
      TabOrder = 1
      ExplicitLeft = 272
      ExplicitTop = 1
      ExplicitHeight = 563
      object pnlTitleLeft: TPanel
        Left = 1
        Top = 1
        Width = 510
        Height = 24
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
        Caption = 'Emplotees'
        TabOrder = 0
      end
      object dbgClient: TDBGrid
        Left = 1
        Top = 25
        Width = 510
        Height = 539
        Align = alClient
        BorderStyle = bsNone
        DataSource = dsEMPLOYEES
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = RUSSIAN_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'FIRST_NAME'
            Title.Caption = 'FirstName'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LAST_NAME'
            Title.Caption = 'LastName'
            Width = 114
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMAIL'
            Title.Caption = 'Email'
            Width = 86
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PHONE_NUMBER'
            Title.Caption = 'Phone'
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALARY'
            Title.Caption = 'Salary'
            Width = 47
            Visible = True
          end>
      end
    end
  end
  object MainMenu: TMainMenu
    Left = 568
    Top = 452
    object miAndmed: TMenuItem
      Tag = 255
      Action = actExit
    end
    object N1: TMenuItem
      Action = actExport
    end
    object miAbi: TMenuItem
      Tag = 255
      Caption = '&Help'
      object miAbout: TMenuItem
        Tag = 255
        Caption = '&About...'
        GroupIndex = 2
        OnClick = miAboutClick
      end
    end
  end
  object alMain: TActionList
    Left = 672
    Top = 456
    object actExport: TAction
      Caption = 'Export'
      OnExecute = actExportExecute
    end
    object actExit: TAction
      Caption = 'Exit'
      OnExecute = actExitExecute
    end
  end
  object adqryDEPARTMENTS: TADQuery
    ObjectView = False
    Connection = dmMain.dbMain
    SQL.Strings = (
      
        'SELECT dep.*, SUM(emp.SALARY) sumSALARY, Count(emp.DEPARTMENT_ID' +
        ') countDep '
      'FROM DEPARTMENTS dep'
      
        'LEFT OUTER JOIN EMPLOYEES emp ON dep.DEPARTMENT_ID = emp.DEPARTM' +
        'ENT_ID'
      'GROUP BY dep.DEPARTMENT_ID'
      'ORDER BY dep.DEPARTMENT_ID')
    Left = 64
    Top = 176
  end
  object adqryEMPLOYEES: TADQuery
    ObjectView = False
    Connection = dmMain.dbMain
    SQL.Strings = (
      'SELECT *'
      'FROM EMPLOYEES'
      'WHERE DEPARTMENT_ID = :ID')
    Left = 64
    Top = 232
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 100
      end>
  end
  object dsEMPLOYEES: TDataSource
    AutoEdit = False
    DataSet = adqryEMPLOYEES
    Left = 168
    Top = 232
  end
end
