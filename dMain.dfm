object dmMain: TdmMain
  OldCreateOrder = False
  Height = 307
  Width = 523
  object dbMain: TADConnection
    Params.Strings = (
      
        'Database=C:\Users\Erik\Documents\RAD Studio\Projects\SimpleSQLit' +
        'e\database.db'
      'LockingMode=Normal'
      'DriverID=SQLite')
    LoginPrompt = False
    Transaction = adtrnsctn1
    Left = 64
    Top = 56
  end
  object ADGUIxErrorDialog: TADGUIxErrorDialog
    Caption = 'FireDAC Executor Error'
    Left = 48
    Top = 128
  end
  object ADGUIxWaitCursor: TADGUIxWaitCursor
    Left = 152
    Top = 128
  end
  object ADPhysSQLiteDriverLink: TADPhysSQLiteDriverLink
    Left = 64
    Top = 8
  end
  object adtrnsctn1: TADTransaction
    Connection = dbMain
    Left = 144
    Top = 64
  end
end
