program SimpleSQLite;

uses
  Vcl.Forms,
  dMain in 'dMain.pas' {dmMain: TDataModule},
  fMain in 'fMain.pas' {frmMain},
  ConstSQLite in 'ConstSQLite.pas',
  uCustomList in 'uCustomList.pas',
  uLoger in 'uLoger.pas',
  uEmployeesList in 'uEmployeesList.pas',
  uCustomSyncList in 'uCustomSyncList.pas',
  uHTMLExport in 'uHTMLExport.pas',
  ThreadBase in 'ThreadBase.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
