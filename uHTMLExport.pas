{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      29-Juni-2014							}
{       Version    1.0                                  }
{       Class writes the data from the list in a file   }
{       in html format                                  }
{*******************************************************}
unit uHTMLExport;

interface
uses  Classes, Windows, sysutils, syncobjs,
			ThreadBase, uEmployeesList, ConstSQLite;

type

	THTMLExport = class(TCustomThread)
  private
    fFileActive: Boolean;
    fLog: TextFile;
    fFileName: String;
    fHandle: THandle;

    function GenDetail(Item: PEmployeesItem): string;
    procedure SetLogName(const Value: String);
    function GenHeader: string;
    procedure EndProgress;
    procedure Progress(Position: integer);
    procedure SendProgress(Value: integer; Tyyp: TProgressType);
    procedure StartProgress(Value: Integer);
  protected
    procedure BreakThread(Sender: TObject); override;
    procedure InternalExec(Sender: TObject); override;
    procedure InternalException(const Status: Cardinal); override;
  public
    constructor	Create(Handle: THandle); reintroduce;
    destructor Destroy; override;
    procedure ShowMessage(Value: String); override;

    procedure WriteLine(const Buf: String);
    procedure FileClose;
    property LogName: String read fFileName write SetLogName;
  end;

var
		HTMLExport: THTMLExport;

implementation
uses uLoger;

{ TLogThread }
constructor THTMLExport.Create(Handle: THandle);
begin
  inherited Create;
  fHandle := Handle;
  ExceptExit := False;
end;

destructor THTMLExport.Destroy;
begin
  FileClose;
  inherited;
end;

procedure THTMLExport.BreakThread(Sender: TObject);
begin
  inherited;
  FileClose;
end;

procedure THTMLExport.InternalException(const Status: Cardinal);
begin
  inherited;
  ShowMessage(fLastException.Message);
end;

function THTMLExport.GenHeader: string;
begin
	Result := '<!DOCTYPE html><html><meta charset="UTF-8"/><title>Exported data</title><style type="text/css">'+
  	'.depRed { color: red; } '+
    '.depBlue { color: blue; } '+
    '.depNormal { color: black; } '+
    'body {	line-height: 1.75em; background-color: #fff; font-size: 11.5pt; color: #353535; }' +
    '</style></head><body>';
end;

function THTMLExport.GenDetail(Item: PEmployeesItem): string;
begin
  with Item^ do
  begin
		Result := '' +
  		Format('<tr><td>%d</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>' +
      	'<td>%s</td><td>%s</td></tr>',
       [EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT,
       MANAGER_ID, DEPARTMENT_ID]);
  end;
end;


procedure THTMLExport.InternalExec(Sender: TObject);
var
		Line: string;
    Size: Integer;
    i: Integer;
    Department: RDepartmentItem;
    HColor: string;
begin
  Size := EmployeesList.Count + 2;
	StartProgress(Size);
  Department := EmployeesList.GetDepartment;
  WriteLine(GenHeader);

  WriteLine('<table class="master">');
  Line := Format('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></TR>',
		['DEPARTMENT_ID', 'DEPARTMENT_NAME', 'MANAGER_ID', 'LOCATION_ID', 'OFFICE_ID']);
  WriteLine(Line);
  Progress(1);

  if EmployeesList.GetAdditionalInfo.Count = 0 then
    HColor := 'depRed'
  else if EmployeesList.GetAdditionalInfo.Salary > MaxSalary then
    HColor := 'depBlue'
  else
  	HColor := 'depNormal';

  with Department do
	  Line := Format('<tr><td>%d</td><td><div class="' + HColor + '">%s</div></td>' +
    	'<td>%s</td><td>%s</td><td>%s</td></TR>',
    	[DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID, OFFICE_ID]);
  WriteLine(Line);
  WriteLine('</table>');
  WriteLine('<table class="detail">');
  Progress(2);

  for i := 0 to Size - 3 do
  begin
    EmployeesList.Lock;
    try
	    Line := GenDetail(EmployeesList.Items[i]);
    finally
			EmployeesList.UnLock;
    end;
    WriteLine(Line);
    Progress(i);
  end;
  WriteLine('</table></body></html>' );

  FileClose;
  EndProgress;
  inherited;
end;

procedure THTMLExport.FileClose;
begin
  if fFileActive then
  begin
	  fFileActive := False;
//	  {$I-}
	  Flush(fLog);
	  CloseFile(fLog);
//	  {$I+}
  end;
end;

procedure THTMLExport.WriteLine(const Buf: String);
begin
  if not fFileActive then
  begin
    AssignFile(fLog, fFileName);
    {$I-}
    Reset(fLog);
    {$I+}
    IOResult;
    Rewrite(fLog);
    fFileActive := True;
  end;

  Writeln(fLog, Buf);
end;

procedure THTMLExport.SetLogName(const Value: String);
begin
  fFileName := Value;
end;

procedure THTMLExport.ShowMessage(Value: String);
begin
	TLoger.Show(Value);
end;

procedure THTMLExport.SendProgress(Value: integer; Tyyp: TProgressType);
var
    pRecInfo: PInfo;
begin
  if FHandle <> 0 then
  begin
    New(pRecInfo);
    pRecInfo.Tyyp := Tyyp;
    pRecInfo.Value := Value;
  	PostMessage(FHandle, PWM_PROGRESS, 0, Integer(pRecInfo));
  end;
end;

procedure THTMLExport.StartProgress(Value: Integer);
begin
  SendProgress(Value, prStart);
end;

procedure THTMLExport.Progress(Position: integer);
Const
			LastTime: Cardinal = 0;
begin
  if (GetTickCount - LastTime > 70) then
  begin
	  SendProgress(Position, prProgress);
    LastTime := GetTickCount;
  end;
end;

procedure THTMLExport.EndProgress;
begin
  SendProgress(0, prEnd);
end;


end.
