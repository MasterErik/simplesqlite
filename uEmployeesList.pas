{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      27-June-2014													}
{       Version    2.0                                  }
{       List to store the exported data									}
{*******************************************************}
unit uEmployeesList;

interface

uses System.SysUtils, System.Classes,
		 uCustomSyncList, uCustomList, ConstSQLite;

type
  EUserListExceprion = class(Exception);

  RDepartmentItem = record
  	DEPARTMENT_ID		: Integer;
  	DEPARTMENT_NAME	: String;
  	MANAGER_ID      : String;
  	LOCATION_ID     : String;
  	OFFICE_ID       : String;
  end;
  PDepartmentItem = ^RDepartmentItem;

  REmployeesItem = record
	  EMPLOYEE_ID		:	Integer;
	  FIRST_NAME		:	String;
	  LAST_NAME			:	String;
	  EMAIL					: String;
	  PHONE_NUMBER	: String;
	  HIRE_DATE     : String;
	  JOB_ID				: String;
	  SALARY        : String;
	  COMMISSION_PCT: String;
  	MANAGER_ID    : String;
	  DEPARTMENT_ID : String;
  end;
  PEmployeesItem = ^REmployeesItem;

  TEmployeesList = class(TCustomSyncList)
  private
	  fDepartment: RDepartmentItem;
    fInfo: RDepartmentInfo;
    function	GetItem(Index: Integer): PEmployeesItem;
  public
    constructor Create; override;
    procedure SetAdditionalInfo(Value: RDepartmentInfo);
    function 	GetAdditionalInfo: RDepartmentInfo;
    procedure SetDepartment(Value: RDepartmentItem);
    function  GetDepartment: RDepartmentItem;
    property	Items[Index: Integer]: PEmployeesItem read GetItem; default;
  end;

Var
		EmployeesList: TEmployeesList = nil;

implementation

Uses uLoger;

function InternalCompare(Item1, Item2: Pointer): Integer;
var
		Value: Integer;
begin
  Value := PEmployeesItem(Item1).EMPLOYEE_ID - PEmployeesItem(Item2).EMPLOYEE_ID;
  if Value > 0 then
  	Result := 1
  else if Value < 0 then
  	Result := -1
  else
  	Result := 0;
end;


{ TEmployeesList }
constructor TEmployeesList.Create;
begin
  inherited;
  fSizeRec := SizeOf(REmployeesItem);
  fFunctionCompare := InternalCompare;
end;


function TEmployeesList.GetDepartment: RDepartmentItem;
begin
  Result := fDepartment;
end;

function TEmployeesList.GetItem(Index: Integer): PEmployeesItem;
begin
  Result := PEmployeesItem(fList[Index]);
end;


procedure TEmployeesList.SetAdditionalInfo(Value: RDepartmentInfo);
begin
	fInfo := Value;
end;

function TEmployeesList.GetAdditionalInfo: RDepartmentInfo;
begin
	Result := fInfo;
end;

procedure TEmployeesList.SetDepartment(Value: RDepartmentItem);
begin
	fDepartment := Value;
end;

end.
