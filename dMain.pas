unit dMain;

interface

uses
  System.SysUtils, System.Classes,
  uADStanIntf, uADGUIxFormsWait, uADGUIxFormsfError, uADStanOption, uADStanError, uADGUIxIntf, uADPhysIntf, uADStanDef,
  uADStanPool, uADStanAsync, uADPhysManager, uADCompGUIx, Data.DB, uADCompClient, uADPhysODBCBase, uADPhysMSSQL,
  uADStanExprFuncs, uADPhysSQLite;

type
  TdmMain = class(TDataModule)
    dbMain: TADConnection;
    ADGUIxErrorDialog: TADGUIxErrorDialog;
    ADGUIxWaitCursor: TADGUIxWaitCursor;
    ADPhysSQLiteDriverLink: TADPhysSQLiteDriverLink;
    adtrnsctn1: TADTransaction;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmMain: TdmMain;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

end.
