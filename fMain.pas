{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      27-June-2014													}
{       Version    1.0                                  }
{       Created by Project ConstSQLLite                }
{*******************************************************}
unit fMain;


interface

uses
  System.Classes, System.SysUtils, Winapi.Messages, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Forms,
  Vcl.ActnList, Vcl.ImgList,  Vcl.Controls, Vcl.Menus, Vcl.StdActns, Vcl.Graphics, Vcl.Dialogs,
  Vcl.ToolWin, Vcl.Buttons, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, Data.DB, System.Types,
  Data.Bind.EngExt, Vcl.Bind.DBEngExt,
  uADStanIntf, uADStanOption, uADStanParam, uADStanError, uADDatSManager, uADPhysIntf, uADDAptIntf, uADStanAsync,
  uADDAptManager, uADCompDataSet, uADCompClient,
  uLoger, ConstSQLite, uEmployeesList, dMain;

type
  TCrackGrid = class (TDBGrid);

  TfrmMain = class(TForm)
    MainMenu: TMainMenu;
    miAndmed: TMenuItem;
    miAbi: TMenuItem;
    miAbout: TMenuItem;
    alMain: TActionList;
    pnlLog: TPanel;
    actExport: TAction;
    N1: TMenuItem;
    actExit: TAction;
    adqryDEPARTMENTS: TADQuery;
    adqryEMPLOYEES: TADQuery;
    dsEMPLOYEES: TDataSource;
    pnlProgress: TPanel;
    pbProgress: TProgressBar;
    Panel4: TPanel;
    pnlMain: TPanel;
    pnlRight: TPanel;
    pnlStatistics: TPanel;
    lvDepartments: TListView;
    pnlUserDetail: TPanel;
    pnlTitleLeft: TPanel;
    dbgClient: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure miAboutClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actExportExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure lvDepartmentsCustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState;
      var DefaultDraw: Boolean);
    procedure lvDepartmentsChange(Sender: TObject; Item: TListItem; Change: TItemChange);
  private
    procedure ThreadWndProc(var msg: TMessage );  message PWM_EVENT;
    procedure ThreadProgress(var msg: TMessage ); message PWM_PROGRESS;

    procedure StartProgress(Value: Integer);
    procedure ShowProgress(Value: integer);
    procedure EndProgress;

    procedure LoadDepartments;
    procedure LoadDepartment(ID: Integer; var Department: RDepartmentItem);
    procedure EmployeesToList(EmployeesList: TEmployeesList);
    procedure DataBaseCurPath(Params: TStrings);
  public
    procedure HandleException(Sender: TObject; E: Exception);
  end;

var
  frmMain: TfrmMain;

implementation
uses uHTMLExport, ThreadBase;

{$R *.DFM}

procedure TfrmMain.ThreadWndProc(var msg: TMessage);
begin
  pnlLog.Caption := GetMsgStr(msg.LParam);
  pnlLog.Visible := True;
end;

procedure TfrmMain.DataBaseCurPath(Params: TStrings);
var
		i: Integer;
    Buf: string;
begin
	Buf := ExtractFileName(Params.Values['DataBase'] );
  Params.Values['DataBase'] := ExtractFilePath(Application.ExeName) + Buf;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  FormatSettings.DecimalSeparator := '.';
  FormatSettings.DateSeparator := '.';
  FormatSettings.ShortDateFormat := 'dd.mm.yyyy';
  TLoger.CreateEx(Handle);
  DataBaseCurPath(dmMain.dbMain.Params);

  Application.OnException := HandleException;
  EmployeesList := TEmployeesList.Create;
  LoadDepartments;
  HTMLExport := THTMLExport.Create(Handle);
  HTMLExport.LogName := 'Export.html';
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  if Assigned(HTMLExport) then
  	ThreadExit(HTMLExport);
  if Assigned(EmployeesList) then
  	FreeAndNil(EmployeesList);
  if Assigned(Loger) then
  	FreeAndNil(Loger);
end;

procedure TfrmMain.LoadDepartments;
var
		ListItem: TListItem;
    PInfo: PDepartmentInfo;
begin
	adqryDEPARTMENTS.Active := True;
  with adqryDEPARTMENTS do
  begin
  	First;
    lvDepartments.Items.BeginUpdate;
    try
      while not eof do
      begin
        New(PInfo);
        PInfo.ID := FieldByName('DEPARTMENT_ID').AsInteger;
        PInfo.Count := FieldByName('countDep').AsInteger;
        PInfo.Salary := FieldByName('sumSALARY').AsCurrency;
        ListItem := lvDepartments.Items.Add;
        ListItem.Data := PInfo;
    	  ListItem.Caption := FieldByName('DEPARTMENT_NAME').AsString;
        Next;
      end;

    finally
			lvDepartments.Items.EndUpdate;
    end;
  end;
end;


procedure TfrmMain.lvDepartmentsChange(Sender: TObject; Item: TListItem; Change: TItemChange);
begin
	if ((Change = ctState) and (Item = lvDepartments.Selected) and lvDepartments.Focused) then
  begin
    adqryEMPLOYEES.Active := False;
    adqryEMPLOYEES.ParamByName('ID').Value := PDepartmentInfo(Item.Data).ID;
    adqryEMPLOYEES.Active := True;
  end;
end;

procedure TfrmMain.lvDepartmentsCustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState;
  var DefaultDraw: Boolean);
begin
	with TCustomListView(Sender) do
  begin
    if not Item.Selected then
    begin
      begin
	      if PDepartmentInfo(Item.Data).Count = 0 then
          Canvas.Font.Color := clRed
        else if PDepartmentInfo(Item.Data).Salary > MaxSalary then
          Canvas.Font.Color := clBlue;
      end;
    end;
  end;
  DefaultDraw := True;
end;

procedure TfrmMain.actExitExecute(Sender: TObject);
begin
	Close;
end;

procedure TfrmMain.LoadDepartment(ID: Integer; var Department: RDepartmentItem);
begin
  with adqryDEPARTMENTS do
		if Locate('DEPARTMENT_ID', ID) then
  	begin
	  	Department.DEPARTMENT_ID		:= FieldByName('DEPARTMENT_ID').AsInteger;
  		Department.DEPARTMENT_NAME	:= FieldByName('DEPARTMENT_NAME').AsString;
	  	Department.MANAGER_ID      	:= FieldByName('MANAGER_ID').AsString;
	  	Department.LOCATION_ID     	:= FieldByName('LOCATION_ID').AsString;
	  	Department.OFFICE_ID       	:= FieldByName('OFFICE_ID').AsString;
	  end;
end;


procedure TfrmMain.EmployeesToList(EmployeesList: TEmployeesList);
var
		Item:	PEmployeesItem;
    DataBookmark: TBookmark;
begin
  with adqryEMPLOYEES do
  begin
  	DisableConstraints;
    dsEMPLOYEES.DataSet := nil;
    DataBookmark := Bookmark;
    try
      First;
      while not Eof do
      begin
        Item := EmployeesList.AllocateItem;
        Item.EMPLOYEE_ID 		:= FieldByName('EMPLOYEE_ID').AsInteger;
        Item.FIRST_NAME   	:= FieldByName('FIRST_NAME').AsString;
        Item.LAST_NAME			:= FieldByName('LAST_NAME').AsString;
        Item.EMAIL					:= FieldByName('EMAIL').AsString;
        Item.PHONE_NUMBER		:= FieldByName('PHONE_NUMBER').AsString;
        Item.HIRE_DATE			:= FieldByName('HIRE_DATE').AsString;
        Item.JOB_ID					:= FieldByName('JOB_ID').AsString;
        Item.SALARY					:= FieldByName('SALARY').AsString;
        Item.COMMISSION_PCT	:= FieldByName('COMMISSION_PCT').AsString;
        Item.MANAGER_ID			:= FieldByName('MANAGER_ID').AsString;
        Item.DEPARTMENT_ID	:= FieldByName('DEPARTMENT_ID').AsString;
        EmployeesList.Add(Item);
        Next;
      end;
    finally
      Bookmark := DataBookmark;
      FreeBookmark(DataBookmark);
      dsEMPLOYEES.DataSet := adqryEMPLOYEES;
      EnableConstraints;
    end;
	end;
end;

procedure TfrmMain.actExportExecute(Sender: TObject);
var
    Department: RDepartmentItem;
    Info: RDepartmentInfo;
begin
    HTMLExport.WaitExecute;
    if adqryEMPLOYEES.Active then
    begin
      EmployeesList.Clear;

      Info := PDepartmentInfo(lvDepartments.Selected.Data)^;
		  LoadDepartment(Info.ID, Department);

		  EmployeesList.SetDepartment(Department);
      EmployeesList.SetAdditionalInfo(Info);
      EmployeesToList(EmployeesList);
    end;
    HTMLExport.Start(nil);
//    TLoger.Show(IntToStr(EmployeesList.Count));
end;


procedure TfrmMain.HandleException(Sender: TObject; E: Exception);
begin
  TLoger.Show(E.Message+' '+IntToStr(E.HelpContext));
end;

procedure TfrmMain.miAboutClick(Sender: TObject);
begin
	ShowMessage('��� �������� ���������.');
end;

procedure TfrmMain.ThreadProgress(var msg: TMessage);
var
    Info: PInfo;
begin
  Info := PInfo(msg.LParam);

  case Info.Tyyp of
    prStart:      StartProgress(Info.Value);
    prProgress:   ShowProgress(Info.Value);
    prEnd:        EndProgress;
  end;
  Dispose(Info);
end;

procedure TfrmMain.StartProgress(Value: Integer);
begin
	pbProgress.Max := Value;
  pnlProgress.Visible := True;
  Application.ProcessMessages;
end;

procedure TfrmMain.ShowProgress(Value: integer);
begin
	pbProgress.Position := Value;
  Application.ProcessMessages;
end;

procedure TFrmMain.EndProgress;
begin
  pbProgress.Position := pbProgress.Max;
end;

end.
