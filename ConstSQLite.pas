{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      04-June-2014													}
{       Version    1.0                                  }
{       Created by Project ConstSQLite                   }
{*******************************************************}
unit ConstSQLite;

interface

uses Winapi.Messages, Winapi.Windows, System.StrUtils, System.SysUtils, System.Classes;

type
	TProgressType = (prStart, prProgress, prEnd);

  RInfo = record
    Value: Integer;
    Tyyp: TProgressType;
  end;
  PInfo = ^RInfo;

  RDepartmentInfo = record
  	ID: Integer;
    Count: Integer;
    Salary: Currency;
  end;
	PDepartmentInfo = ^RDepartmentInfo;

	function GetMsgStr(Value: Integer): String;
	function SetMsgStr(Value: String): Integer;

	procedure StrToList(const S, Separator: string; var Strs: TArray<String>; i: Integer = 0); overload;
	procedure StrToList(const S, Separator: string; var Strs: TStrings; i: Integer =  0); overload;
  function	IIF(condition: Boolean; Val1, Val2: Integer): Integer;

const
  PWM_PROGRESS = WM_USER + 200 + 1;
	prError = -MaxInt;
  MaxSalary = 20000;

implementation

function GetMsgStr(Value: Integer): String;
var
  P: PChar;
begin
  P := PChar(Value);
  Result := StrPas(P);
  FreeMem(P);
end;

function SetMsgStr(Value: String): Integer;
var
  P: PChar;
begin
  GetMem(P, Length(Value) * SizeOf(Char) + 1 * SizeOf(Char));
  StrCopy(P, PChar(Value));
  Result := Integer(P);
end;

procedure StrToList(const S, Separator: string; var Strs: TArray<String>; i: Integer = 0);
var
  Posit, Size, j: Integer;
  Buf: string;
begin
  if Length(S) > 0 then
  begin
    Posit := 1;
    SetLength(Strs, 5);
    j := 0;
    while (Posit > 0) do
    begin
      Posit := PosEx(S, Separator, i + 1);
      if Posit = 0 then
        Size := Length(S)
      else
        Size := Posit - i - 1;

      Buf := Trim(Copy(S, i + 1, Size));
      i := Posit;
  //    if Buf <> '' then
  //    begin
        if j > Length(Strs) - 1 then
          SetLength(Strs, j + 5);
        Strs[j] := Buf;
        Inc(j);
  //    end;
    end;
    SetLength(Strs, j);
  end;
end;

procedure StrToList(const S, Separator: string; var Strs: TStrings; i: Integer =  0);
var
  Posit, Size: Integer;
  Buf: string;
begin
  Posit := 1;
  Strs := TStringList.Create;

  Strs.BeginUpdate;
  while (Posit > 0) do
  begin
    Posit := PosEx(S, Separator, i + 1);
    if Posit = 0 then
      Size := Length(S)
    else
      Size := Posit - i - 1;

    Buf := Trim(Copy(S, i + 1, Size));
    i := Posit;
    if Buf <> '' then
      Strs.Add(Buf);
  end;
  Strs.EndUpdate;
end;

function	IIF(condition: Boolean; Val1, Val2: Integer): Integer;
begin
	if condition then
      Result := Val1
  else
		Result := Val2;

end;


end.
